#pragma once


#include "stdafx.h"


class ProcParams {
	string trackedImagePath;
	string backgroundImagePath;

public:
	ProcParams(const string backgroundImagePath, const string trackedImagePath)
		:backgroundImagePath(backgroundImagePath), trackedImagePath(trackedImagePath) {}

	const string getTrackedImagePath() {
		return trackedImagePath;
	}

	const string getBackgroundImagePath() {
		return backgroundImagePath;
	}
};


class OrderedVec3b // class used to create an ordering rule for Vec3b values
{
public:
	Vec3b coordinate;

	OrderedVec3b(Vec3b vec) {
		this->coordinate = vec;
	}

	const bool operator<(const OrderedVec3b &coordinate) const {
		return (this->coordinate[1] + this->coordinate[1] + this->coordinate[2]) / 3 <
			(coordinate.coordinate[0] + coordinate.coordinate[1] + coordinate.coordinate[2]) / 3;
	}
};


int launchProc(Mat& inImg, char inProcId, ProcParams& inParams, Mat& outImg);

int procEdgeEnhancing(Mat& inImg, Mat& outImg);
int procThreshold(Mat& inImg, string inBkgImgPath, Mat& outImg);
int procTimeshift(Mat& inImg, Mat& outImg);
int procImgAveraging(Mat& inImg, Mat& outImg);
int procTracking(Mat& inImg, string inTrackedImgPath, Mat& outImg);
int procImgMedian(Mat& inImg, Mat& outImg);



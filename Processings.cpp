#include "Processings.h"


/* Main switch to choose the type of process. */
int launchProc(Mat& inImg, char inProcId, ProcParams& inParams,  Mat& outImg) {
	switch (inProcId) {
	case '1':
		return procEdgeEnhancing(inImg, outImg);
	case '2':
		return procThreshold(inImg, inParams.getBackgroundImagePath(), outImg);
	case '3':
		return procTimeshift(inImg, outImg);
	case '4':
		return procImgAveraging(inImg, outImg);
	case '5':
		return procTracking(inImg, inParams.getTrackedImagePath(), outImg);
	case '6':
		return procImgMedian(inImg, outImg);
	default:
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}


/* This functions does a contour detection, and then masks input image
to remove all contour pixels. Looks a little like a cartoon. */
int procEdgeEnhancing(Mat& inImg, Mat& outImg) {

	// Denoising and computing gradient of input image
	Mat edges, filtImg;
	cvtColor(inImg, edges, CV_BGR2GRAY);
	bilateralFilter(inImg, filtImg, 10, 30, 10);
	Canny(filtImg, edges, 1000, 1000, 5);
	
	bitwise_not(edges, edges);
	erode(edges, edges, Mat::ones(2, 2, edges.type()), Point(1, 1), 1, 0, 0);
	
	outImg = Mat::zeros(inImg.size(), inImg.type());
	filtImg.copyTo(outImg, edges);
	
	return EXIT_SUCCESS;
}


/* This function looks for all green objects, and replaces these objects with another image. */
int procThreshold(Mat& inImg, string inBkgImgPath, Mat& outImg) {

	Mat binImg, hsvImg;

	// Blurring to remove noise
	GaussianBlur(inImg, hsvImg, Size(7, 7), 3, 3);

	// Color threshold on HSV image (to get green)
	Mat maskImg;
	cvtColor(hsvImg, hsvImg, CV_BGR2HSV);
	inRange(hsvImg, Scalar(38, 50, 50), Scalar(75, 200, 255), maskImg);

	// Median filtering to remove noise on green pixels image
	medianBlur(maskImg, maskImg, 7);

	// Image to use in transparency
	Mat imgHidden;
	static Mat imgHiddenResized;
	if (!imgHiddenResized.data) {
		imgHidden = imread(inBkgImgPath);
		resize(imgHidden, imgHiddenResized, inImg.size());
	}
	
	// Inverting mask
	Mat maskImgInv;
	bitwise_not(maskImg, maskImgInv);

	// Creating combined image
	Mat img1, img2;
	inImg.copyTo(img1, maskImgInv);
	imgHiddenResized.copyTo(img2, maskImg);
	add(img1, img2, outImg);

	return EXIT_SUCCESS;
}


// This function applies a linear delay (in number of images) to lines of the input.
// Delay is 0 for the first line, and inImg.rows*nbImgShift images for the last one.
int procTimeshift(Mat& inImg, Mat& outImg) {

	double nbImgShift = 0.1; // delay in number of images between two lines
	int nbImg = (int) (inImg.rows * nbImgShift); // number of required previous images to transform all lines

	static int curImgIndex = 0; // indicates the index of last image
	static Mat *tabImg = (Mat*) calloc(nbImg, sizeof(*tabImg)); // allocation of output image

	inImg.copyTo(tabImg[curImgIndex]);

	// First call: we need to allocate outImg
	if (outImg.empty()) {
		outImg = Mat(inImg.size(), inImg.type());
	}

	for (int i = 0; i < inImg.rows; i++) {
		int curImgShifted;

		// Looking for index of previous image corresponding to current line
		if (curImgIndex - (int)(i*nbImgShift) < 0) {
			curImgShifted = nbImg + (curImgIndex - (int)(i*nbImgShift));
		}
		else if (curImgIndex - (int)(i*nbImgShift) < nbImg) {
			curImgShifted = curImgIndex - (int)(i*nbImgShift);
		}

		// Copying line to output image
		if (!tabImg[curImgShifted].empty()) {
			tabImg[curImgShifted](Rect(0, i, inImg.cols, 1)).copyTo(outImg(Rect(0, i, inImg.cols, 1)));
		}
	}

	curImgIndex = curImgIndex >= nbImg-1 ? 0 : ++curImgIndex; // pointing to next image for next iteration

	return EXIT_SUCCESS;
}


// This function computes the average of the nbImgsMean last images
int procImgAveraging(Mat& inImg, Mat& outImg) {

	int nbImgsMean = 10; // number of averaged images

	static int curImgIndex = 0; // indicates the index of last image
	static Mat *tabImg = (Mat*)calloc(nbImgsMean, sizeof(*tabImg)); // allocation of table of last images

	static Mat sumImg = Mat::zeros(inImg.size(), CV_32SC3);			// Image containing the sum of the nbImgsMean images

	// Oldest image is removed from table
	if (!tabImg[curImgIndex].empty()) {
		subtract(sumImg, tabImg[curImgIndex], sumImg, noArray(), CV_32SC3);
	}

	// Current input image is added to table
	inImg.copyTo(tabImg[curImgIndex]);

	// Newest image is added to sum
	add(sumImg, inImg, sumImg, noArray(), CV_32SC3);

	// Normalization of mean image
	static Mat imgBGR_Ones = Mat(inImg.size(), CV_8UC3, Scalar(1, 1, 1)); // to avoid allocation at every call of procImgAveraging()
	multiply(sumImg, imgBGR_Ones, outImg, 1.0 / nbImgsMean, CV_8UC3);

	// Incrementation of next image pointer
	curImgIndex = curImgIndex >= nbImgsMean - 1 ? 0 : curImgIndex + 1;

	return EXIT_SUCCESS;
}


// This function computes the median of the nbImgsMedian last images
int procImgMedian(Mat& inImg, Mat& outImg) {

	int nbImgsMedian = 10; // number of images used to compute median

	static int curImgIndex = 0; // indicates the index of last image
	static Mat *tabImg = nullptr;  // table of last images
	
	if (tabImg == nullptr) {
		// Allocation of table of last images
		tabImg = (Mat*)calloc(nbImgsMedian, sizeof(*tabImg));

		// Initialization of image list
		for (int k = 0; k < nbImgsMedian; k++) {
			tabImg[k] = Mat(inImg.size(), CV_8UC3, Scalar(0, 0, 0));
		}
	}

	// Current input image is added to table
	inImg.copyTo(tabImg[curImgIndex]);

	// First call: we need to allocate outImg
	if (outImg.empty()) {
		outImg = Mat(inImg.size(), inImg.type());
	}

	#pragma omp parallel for
	for (int j = 0; j < inImg.rows; j++) {
		for (int i = 0; i < inImg.cols; i++) {
			
			// Getting list of intensities for current pixel
			vector<OrderedVec3b> listValues;
			listValues.reserve(nbImgsMedian);
			for (int k = 0; k < nbImgsMedian; k++) {
				listValues.push_back(OrderedVec3b(tabImg[k].at<Vec3b>(Point(i, j))));
			}

			// Getting median value
			nth_element(listValues.begin(), listValues.begin() + listValues.size() / 2, listValues.end());
			OrderedVec3b medianValue = listValues[listValues.size() / 2];

			// Setting value in output image
			outImg.row(j).col(i) = medianValue.coordinate;
		}
	}

	// Incrementation of next image pointer
	curImgIndex = curImgIndex >= nbImgsMedian - 1 ? 0 : curImgIndex + 1;

	return EXIT_SUCCESS;
}


/* This function does an AKAZE feature detection, description, then matching of a sample image
with inImg.
*/
int procTracking(Mat& inImg, string inTrackedImgPath, Mat& outImg) {

	const float nn_match_ratio = 0.5f;   // Nearest neighbor matching ratio
	static Mat imgObject = imread(inTrackedImgPath);
	
	// Detecting keypoints and computing descriptors
	Ptr<AKAZE> descriptor = AKAZE::create(5, 0, 3, 0.0005F, 4, 4, 1);

	static Mat desc1;
	static std::vector<KeyPoint> keypoints_1;
	if (keypoints_1.empty()) {
		// Image of tracked object is processed only 1 time
		descriptor->detectAndCompute(imgObject, noArray(), keypoints_1, desc1);
	}
	Mat desc2;
	std::vector<KeyPoint> keypoints_2;
	descriptor->detectAndCompute(inImg, noArray(), keypoints_2, desc2);

	// Brute force matching
	BFMatcher matcher(NORM_HAMMING);
	vector< vector<DMatch> > nn_matches;
	matcher.knnMatch(desc1, desc2, nn_matches, 2);

	vector<KeyPoint> matched1, matched2;
	vector<DMatch> good_matches;

	for (int i = 0; i < nn_matches.size(); i++) {
		DMatch first = nn_matches[i][0];
		float dist1 = nn_matches[i][0].distance;
		float dist2 = nn_matches[i][1].distance;

		if (dist1 < nn_match_ratio * dist2) {
			int new_i = (int) matched1.size();
			matched1.push_back(keypoints_1[first.queryIdx]);
			matched2.push_back(keypoints_2[first.trainIdx]);
			good_matches.push_back(DMatch(new_i, new_i, 0));
		}
	}

	// If no match was found, exit
	if (matched1.size() < 8) {
		return EXIT_FAILURE;
	}

	drawMatches(imgObject, matched1, inImg, matched2, good_matches, outImg);

	// Computing perspective transformation
	vector<Point2f> pts1, pts2;
	for (int i = 0; i < matched1.size(); i++) {
		pts1.push_back(matched1[i].pt);
		pts2.push_back(matched2[i].pt);
	}
	Mat perspectiveTransform = findHomography(pts1, pts2, noArray());

	// Warping
	Mat resImg;
	warpPerspective(imgObject, resImg, perspectiveTransform, inImg.size());
	imshow("Warped image", resImg);

	return EXIT_SUCCESS;
}



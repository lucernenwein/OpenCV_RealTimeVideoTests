// TestVideo.cpp: program entry point 
//

#include "stdafx.h"
#include "Processings.h"
#include "GUI.h"

int main(int argc, char** argv) {

	// Checking input parameters
	if (argc != 3) {
		cout << "Usage: TestVideo <pathToGreenScreenImage> <pathToTrackingImage>" << endl;
		cout << "\tpathToGreenScreenImage: background image of processing n 2" << endl;
		cout << "\tpathToTrackedImage: image to track in processing n 5" << endl;
		return EXIT_FAILURE;
	}

	ProcParams params(argv[1], argv[2]);

	// Opening video streaming from camera
	VideoCapture stream1(0);

	if (!stream1.isOpened()) { //check if video device has been initialized
		cout << "Cannot open camera";
		return EXIT_FAILURE;
	}

	char pressedKey = 'a'; // last pressed key
	char curProcType = '1'; // last selected processing type

	Mat cameraFrame;
	Mat resultImg;

	int nbFPS = 0;
	
	while (pressedKey != 'q') {
		// Reading current image
		stream1.read(cameraFrame);

		launchProc(cameraFrame, curProcType, params, resultImg);

		// Displaying number of FPS
		putText(cameraFrame, "FPS:" + to_string(nbFPS), Point(5, 15), FONT_HERSHEY_PLAIN, 1, Scalar(0, 255, 255));

		imshow("Camera output", cameraFrame);
		imshow("Camera output processed", resultImg);

		// Set the callback function for any mouse event
		setMouseCallback("Camera output", CallBackFunc, &cameraFrame);
		setMouseCallback("Camera output processed", CallBackFunc, &resultImg);

		// Getting the key pressed by the user
		pressedKey = waitKey(1);
		if (pressedKey >= '1' && pressedKey <= '6') {
			curProcType = pressedKey;
		}
		
		updateFPS(nbFPS);
	}

	stream1.release();

	return EXIT_SUCCESS;
}

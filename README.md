# Realtime video processing using OpenCV

The goal of this project was to explore some of OpenCV's capabilities when it comes to realtime video processing, image segmentation and feature detection. I'm using a webcam as my acquisition device, which gives low quality images but is enough to tinker around. I coded 5 different algorithms, which are described below.

## 1 - Object tracking

Here I'm using a feature detection and pairing technique to register a pattern on the acquired image. The feature descriptor used is AKAZE, which is adapted for realtime purposes as it's a binary descriptor: these types of descriptors are computed far faster than HOG ones (like SIFT).

The descriptors are computed once on the pattern P, and then they're compared to the ones on the current acquired image A using the Hamming distance. Points of A that have a similar smallest distance to two points in P are considered outliers and ignored.

Finally, I compute the homography between the paired sets of points in P and A, and use this transformation to warp the pattern. 

![Traked pattern](Images/tracked_pattern.png)*Tracked pattern: An image I found using Google*

![Tracking example 1](Images/process_tracking1.png)*Example 1: partially blocked view of the cartoon strip*

![Tracking example 2](Images/process_tracking2.png)*Example 2: extreme close-up of the cartoon strip*

## 2 - Linear time shift

Here, I simply apply a linear delay (in number of acquired images) which grows from top to bottom of the image. Delay is 0 for the first row, NbRows*NbOfImagesShift images for the last one.

The blocky artefacts are due to the acquisition rate of the camera: at 30 FPS, it takes 16s to refresh the entire image line by line (using a different image for each line), but just 8s to refresh it 2 lines by 2 lines, etc. In this example I choosed to refresh the image by blocks of 10 lines to not have too much delay.

![Linear time shift](Images/process_linear_timeshift.png)

## 3 - Time average

This is pretty straightforward, I just display the average of the N last images. It is worth noting that the average is computed in constant time once the N-th image has been acquired, which is done by using a buffer storing the sum of the N last images. When a new image is acquired, all I need to do is subtract the oldest image from the buffer, add the new one and finally divide by N. 

![Time average](Images/process_time_average.png)

## 4 - Cartoon transform

This process makes the image look like a cartoon. The main idea here is to smooth out regions of similar intensity while preserving the edges (using a bilateral filter). I then use a Canny edges detector to detect the edge pixels, and finally set them to 0 in the filtered image (result on the right).

![Cartoon](Images/process_cartoon.png)

## 5 - Greenscreen

Just a classical green screen algorithm, where all green pixels get replaced by another image. The trick is to use the HSV representation of the image, in which it is very easy to segment a single color. Some minor filtering to remove noise is also done using a gaussian filter (on the color image) and a median filter (on thresholded green pixels image).

![greenscreen](Images/process_greenscreen.png)
#include "GUI.h"


// Function called when a mouse event happens.
void CallBackFunc(int event, int x, int y, int flags, void* userdata) {
	
	if (event == EVENT_LBUTTONDOWN) {
		cout << "Left button - position (" << x << ", " << y << ")" << endl;
	}
	else if (event == EVENT_RBUTTONDOWN) {
		cout << "Right button - position (" << x << ", " << y << ")" << endl;
	}
	else if (event == EVENT_MBUTTONDOWN) {
		cout << "Middle button - position (" << x << ", " << y << ")" << endl;
	}
	else if (event == EVENT_MOUSEMOVE) {
		Mat* curImg = (Mat*)userdata;
		try {
			Vec3b ptVal = curImg->at<Vec3b>(y, x);
			cout << "Mouse move - position (" << x << ", " << y << ")" << "  Value (" << (int)ptVal.val[0] << ", " << (int)ptVal.val[1] << ", " << (int)ptVal.val[2] << ")" << endl;
		}
		catch (Exception except) {}
	}
}


// This function counts how many times it was called in the last second.
// Call it everytime you have computed a new image.
void updateFPS(int &nbFPS) {

	static time_t lastTime = time(nullptr);
	static int nbFrames = 0;
	
	time_t curTime = 0;
	double elapsedTime = 0;
	
	nbFrames++;
	time(&curTime);
	elapsedTime = difftime(curTime, lastTime);
	if (elapsedTime >= 1) {
		nbFPS = nbFrames;
		nbFrames = 0;
		lastTime = curTime;
	}
}
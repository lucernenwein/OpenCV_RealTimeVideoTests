MYSRC=GUI.cpp		Processings.cpp	TestVideo.cpp	stdafx.cpp
MYOBJ=$(MYSRC:.cpp=.o)
LOADLIBES=$(MYOBJ)
LDLIBS=-L/opt/local/lib -lopencv_core -lopencv_imgcodecs -lopencv_highgui -lopencv_imgproc -lopencv_videoio -lopencv_calib3d -lopencv_features2d

ifeq ($(OS),Windows_NT)
	uname_S:=Windows
else
# for complete check use: gcc -dumpmachine
	uname_S:=$(shell uname -s)
endif

ifeq ($(uname_S), Linux)
# Linux
# zypper in opencv 
	OUTPUT_OPTION=-I/opt/local/include -std=c++0x -O3
endif

ifeq ($(uname_S), Darwin)
# OSX 
# port install opencv
#
#more optimized version exemple
#/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang -cc1 -triple x86_64-apple-macosx10.11.0 -Wdeprecated-objc-isa-usage -Werror=deprecated-objc-isa-usage -emit-obj -disable-free -disable-llvm-verifier -main-file-name qpeldsp.c -mrelocation-model pic -pic-level 2 -mthread-model posix -fno-signed-zeros -masm-verbose -munwind-tables -target-cpu core2 -target-linker-version 264.3.102 -dwarf-column-info -debug-info-kind=standalone -dwarf-version=2 -coverage-file /opt/local/var/macports/build/_opt_local_var_macports_sources_rsync.macports.org_release_tarballs_ports_multimedia_ffmpeg/ffmpeg/work/ffmpeg-3.1.3/libavcodec/qpeldsp.o -resource-dir /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/../lib/clang/7.3.0 -dependency-file libavcodec/qpeldsp.d -MT libavcodec/qpeldsp.o -I . -I ./ -I /opt/local/include -D _ISOC99_SOURCE -D _FILE_OFFSET_BITS=64 -D _LARGEFILE_SOURCE -I ./compat/dispatch_semaphore -D PIC -D ZLIB_CONST -D HAVE_AV_CONFIG_H -D HAVE_LRINTF -I /opt/local/include -I /opt/local/include -I /opt/local/include/p11-kit-1 -I /opt/local/include -I /opt/local/include -I /opt/local/include/freetype2 -I /opt/local/include -I /opt/local/include/fribidi -I /opt/local/include/glib-2.0 -I /opt/local/lib/glib-2.0/include -I /opt/local/include -I /opt/local/include/freetype2 -I /opt/local/include -I /opt/local/include -I /opt/local/include/freetype2 -I /opt/local/include -I /opt/local/include/freetype2 -I /opt/local/include/freetype2 -I /opt/local/include/fribidi -I /opt/local/include/glib-2.0 -I /opt/local/lib/glib-2.0/include -I /opt/local/include -I /opt/local/include -I /opt/local/include/opus -I /opt/local/include/schroedinger-1.0 -I /opt/local/include/orc-0.4 -I /opt/local/include -I /opt/local/include -I /opt/local/include -I /opt/local/include -I /opt/local/include -I /opt/local/include -D _GNU_SOURCE=1 -D _THREAD_SAFE -I /opt/local/include/SDL -I /opt/local/include -I /opt/local/include -I /opt/local/include -I /opt/local/include -I/opt/local/include -O3 -Wdeclaration-after-statement -Wall -Wdisabled-optimization -Wpointer-arith -Wredundant-decls -Wwrite-strings -Wtype-limits -Wundef -Wmissing-prototypes -Wno-pointer-to-int-cast -Wstrict-prototypes -Wempty-body -Wno-parentheses -Wno-switch -Wno-format-zero-length -Wno-pointer-sign -Wno-unused-const-variable -Werror=implicit-function-declaration -Werror=missing-prototypes -Werror=return-type -std=c99 -fconst-strings -fdebug-compilation-dir /opt/local/var/macports/build/_opt_local_var_macports_sources_rsync.macports.org_release_tarballs_ports_multimedia_ffmpeg/ffmpeg/work/ffmpeg-3.1.3 -ferror-limit 19 -fmessage-length 0 -pthread -stack-protector 1 -fblocks -fobjc-runtime=macosx-10.11.0 -fencode-extended-block-signature -fmax-type-align=16 -fdiagnostics-show-option -vectorize-loops -vectorize-slp -o libavcodec/qpeldsp.o -x c libavcodec/qpeldsp.c
	COMPILE.cpp=clang++ -c
	LINK.cpp=clang++
	OUTPUT_OPTION=-I/opt/local/include -std=c++0x -O3
endif

TestVideo: $(MYOBJ)
	$(LINK.cpp) -v -o $@ $(MYOBJ) $(LDLIBS)

clean:
	rm -f $(MYOBJ)
